# xmem
    一个轻量级的内存管理组件，支持内存动态分配、回收与越界检测，只需简单的配置就能轻松导入工程，使用及其方便，可以有效地提高研发测试效率。
	同时，xmem支持多种个性化操作，可以更高效更便捷的管理内存。
    xmem的设计支持Header分离模式，可以配置XMEM_HEADER_DIVIDE使能Header分离模式。Header分离模式内存块管理头与内存块采用相向生长模式，避免因内存写越界，内存块管理头信息出错而导致整个程序崩溃的问题。
    xmem支持超块（super block）,可以配置XMEM_SUPERBLOCK_SUPPORT使能Super Block功能。使用一个头管理多块小内存(cell)，这样可以大大减小头开销。
    xmem支持越界检测, 可以配置XMEM_CORRUPT_CHECK开启越界检测。共Header模式检查Header的值是否合法，Header分离模式检查各内存块的barrier。

Ultra-lightweight dynamic memory allocation in C.

## How to use
    1. 配置临界区进入宏XMEM_ENTER_CRITICAL，与临界区退出宏XMEM_EXIT_CRITICAL。
    2. 在内存里定义一个数组，用作xmem内存池。
    3. 调用xMemInit初始化xmem。
    4. 调用xmalloc分配内存，调用xfree释放内存。
    ...
### License

Apache License V2.0

