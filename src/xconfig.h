/**
 * @file xconfig.h
 * @brief 
 * @author Gavin (420260138@qq.com)
 * @version 1.1
 * @date 2020-07-20
 * 
 * @copyright Copyright (c) {2020}  Gavin.Hsu
 *  
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of Apache License Version 2.0
 * 
 * @par 修改日志:
 * <table>
 * <tr><th>Date       <th>Version <th>Author  <th>Description
 * <tr><td>2020-07-20 <td>1.1     <td>Gavin     <td>update
 * </table>
 */
#ifndef __XCONFIG_H__
#define __XCONFIG_H__


#define XMEM_HAS_ASSERT        0

#define XMEM_DEBUG_ENABLE    1

#define XMEM_CPU_64BIT    1

#define XMEM_SUPERBLOCK_SUPPORT    1


/*****************************************************************************
*MACRO 
*XMEM_CORRUPT_CHECK 
*DESCRIPTION
*corrupt check, for debug purpose
*0 no corrupt check
*1 do corrupt check 
*******************************************************************************/
#define XMEM_CORRUPT_CHECK      0

/*****************************************************************************
*MACRO 
*XMEM_HEADER_DIVIDE 
*DESCRIPTION
*divide header and memory block
*0
*--------------------------------------------------------------
*|       |     prev     |     blk      |    next      |       |
*|  ...  |--------------|--------------|--------------|  ...  |
*|       |header|  mem  |header|  mem  |header|  mem  |       |
*--------------------------------------------------------------
*1
*------------------------------------------------------------------------------
*|Header List|  xMemHdrEnd--->|  ...   |<---xMemBlkStart  |Blocks,Super Blocks|
*------------------------------------------------------------------------------
*******************************************************************************/
#define XMEM_HEADER_DIVIDE      1

//alignment
#if XMEM_CPU_64BIT
#define XMEM_ALIGNMENT_SIZE     (8)
#else
#define XMEM_ALIGNMENT_SIZE     (4)
#endif// end of XMEM_CPU_64BIT

/******************************************************************************************
 * on 32-bit cpu, xMemMgrHdr requires 8 bytes, xMemBlock requires 16 bytes, a block consumes
 * 24 bytes for a header
 *
 * on 64-bit cpu, xMemMgrHdr requires 12 bytes, xMemBlock requires 24 bytes, a block consumes
 * 36 bytes for a header
 *
 * ballance size determined waste size when a free block lager than requrired
*******************************************************************************************/
#define XMEM_BALLANCE_SIZE    (XMEM_CELL_SIZE*4)

#if XMEM_CPU_64BIT
#define XMEM_BARRIER_SIZE     8
#else
#define XMEM_BARRIER_SIZE     4
#endif // end of XMEM_CPU_64BIT

#if XMEM_CPU_64BIT
#define XMEM_BARRIER_VALUE    0x55AA55AA55AA55AA
#else
#define XMEM_BARRIER_VALUE    0x55AA55AA
#endif // end of XMEM_CPU_64BIT

#define XMEM_CELL_SIZE     XMEM_ALIGNMENT_SIZE
#define XMEM_2CELL_SIZE    (XMEM_CELL_SIZE*2)
#define XMEM_4CELL_SIZE    (XMEM_CELL_SIZE*4)
#define XMEM_8CELL_SIZE    (XMEM_CELL_SIZE*8)

#define XMEM_SUPERBLOCK_1CELL_CNT        (16)
#define XMEM_SUPERBLOCK_2CELL_CNT        (32)
#define XMEM_SUPERBLOCK_4CELL_CNT        (16)

#if XMEM_CPU_64BIT
#define XMEM_SUPERBLOCK_8CELL_CNT        (0)
#else
#define XMEM_SUPERBLOCK_8CELL_CNT        (16)
#endif // end of XMEM_CPU_64BIT

#if XMEM_CPU_64BIT
#define XMEM_SUPERBLOCK_CTRLSIZE      8
#define XMEM_SUPERBLOCK_CTRLBITS      (XMEM_SUPERBLOCK_CTRLSIZE*8)
#define XMEM_SUPERBLOCK_CTRLALL       (0xFFFFFFFFFFFFFFFF)
#else
#define XMEM_SUPERBLOCK_CTRLSIZE      4
#define XMEM_SUPERBLOCK_CTRLBITS      (XMEM_SUPERBLOCK_CTRLSIZE*8)
#define XMEM_SUPERBLOCK_CTRLALL       (0xFFFFFFFF)
#endif // end of XMEM_CPU_64BIT

#endif // __XCONFIG_H__
