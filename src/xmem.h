/**
 * @file xmem.h
 * @brief 
 * @author Gavin (420260138@qq.com)
 * @version 1.0
 * @date 2020-07-20
 * 
 * @copyright Copyright (c) {2020}  Gavin.Hsu
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of Apache License Version 2.0
 * 
 * @par 修改日志:
 * <table>
 * <tr><th>Date       <th>Version <th>Author  <th>Description
 * <tr><td>2020-07-20 <td>1.0     <td>Gavin     <td>create
 * </table>
 */
#ifndef __XMEM_H__
#define __XMEM_H__

/**
 * @brief allocate a block of memory
 * @param  size             memory size required
 * @return void* 
 */
void * xmalloc( size_t size );
/**
 * @brief free a block of memory
 * @param  ptr              pointer to a block  of memory to be free
 */
void xfree( void * ptr );
/**
 * @brief Init X-Memory Pool
 * @param  addr             memory pool address
 * @param  size             memeory pool size in bytes
 */
void xMemInit( AddressType addr, size_t size );
/**
 * @brief dump memory information
 */
void xMemInfoDump( void );
/**
 * @brief corrupt check
 */
void xMemCorruptCheck( void );

#endif // end of __XMEM_H__
