/**
 * @file utilities.h
 * @brief 
 * @author Gavin (420260138@qq.com)
 * @version 1.0
 * @date 2020-07-20
 * 
 * @copyright Copyright (c) {2020}  Gavin.Hsu
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of Apache License Version 2.0
 * 
 * @par 修改日志:
 * <table>
 * <tr><th>Date       <th>Version <th>Author  <th>Description
 * <tr><td>2020-07-20 <td>1.0     <td>Gavin     <td>create
 * </table>
 */

#ifndef __UTILITIES_H__
#define __UTILITIES_H__

/**
 * @brief 
 * @param  msg              message
 * @param  data             pointer to dump data 
 * @param  size             dump data length in bytes
 */
void xMemDump( const char * msg, unsigned char * data, size_t size );
/**
 * @brief 
 * @param  xMemHdrLst       xMem Manage Header List to dump
 */
void xMemMgrHdrListInfoDump( pxMemMgrHdr xMemHdrLst );
/**
 * @brief 
 * @param  xMemSupBlkLst    xMem Superblock List to dump
 * @param  len              xMem Superblock List length
 */
void xMemSuperBlockInfoDump( xMemSuperBlock ** xMemSupBlkLst, int len );
/**
 * @brief 
 * @param  xMemBlkLst       xMem Block List to dump
 */
void xMemBlockListInfoDump( pxMemBlock xMemBlkLst );
#endif // end of __UTILITIES_H__
