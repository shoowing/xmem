/**
 * @file platform.h
 * @brief 
 * @author Gavin (420260138@qq.com)
 * @version 1.1
 * @date 2020-07-20
 * 
 * @copyright Copyright (c) {2020}  Gavin.Hsu
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of Apache License Version 2.0
 * 
 * @par 修改日志:
 * <table>
 * <tr><th>Date       <th>Version <th>Author  <th>Description
 * <tr><td>2020-07-20 <td>1.1     <td>Gavin     <td>add assert definition
 * </table>
 */
#ifndef __PLATFORM_H__
#define __PLATFORM_H__

#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>

#if XMEM_HAS_ASSERT
#define xMemAssert assert
#else
#define xMemAssert(x) \
                if(!(x)){ \
                    printf("assert at %s, %d\r\n",__FILE__,__LINE__);\
                    while(1); \
                }
#endif // end of XMEM_HAS_ASSERT

#if XMEM_DEBUG_ENABLE
#define xMemDebugPrintf  printf
#else
#define xMemDebugPrintf(...)
#endif // end of XMEM_DEBUG_ENABLE


#define xMemInfoPrintf    printf

#define XMEM_ENTER_CRITICAL()  
        
#define XMEM_EXIT_CRITICAL() 

#endif // end of __PLATFORM_H__
