/**
 * @file xtypes.h
 * @brief 
 * @author Gavin (420260138@qq.com)
 * @version 1.1
 * @date 2020-07-20
 * 
 * @copyright Copyright (c) {2020}  Gavin.Hsu
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of Apache License Version 2.0
 * 
 * @par 修改日志:
 * <table>
 * <tr><th>Date       <th>Version <th>Author  <th>Description
 * <tr><td>2020-07-20 <td>1.1     <td>Gavin     <td>update
 * </table>
 */
#ifndef __XTYPES_H__
#define __XTYPES_H__

#include "xconfig.h"

#if XMEM_CPU_64BIT == 1
typedef unsigned long long AddressType;
#else
typedef unsigned long AddressType;
#endif

typedef unsigned long long u64;
typedef int s32;
typedef unsigned int u32;
typedef short s16;
typedef unsigned short u16;
typedef char s8;
typedef unsigned char u8;

#define __ARRAY_EMPTY

#define XMEM_ALIGN(x,n) (((x) + ((n)-1)) & ~((n)-1))

#define XMEM_ATTR_PACKED __attribute((packed))
#define XMEM_ATTR_ALIGNED_4 __attribute((aligned(4)))

#if XMEM_CPU_64BIT == 1
#define RAMADDR_PRINTF_XFMT "0x%016llx"
#else
#define RAMADDR_PRINTF_XFMT "0x%08x"
#endif // end of XMEM_CPU_64BIT

enum{
    XMEM_LIST_TYPE_FREE=0,
    XMEM_LIST_TYPE_BLOCK,
    XMEM_LIST_TYPE_SUPERBLOCK,
    XMEM_LIST_TYPE_MAX
};

typedef struct XMEM_ATTR_PACKED XMEM_ATTR_ALIGNED_4  t_xMemManagementHeader{
    struct t_xMemManagementHeader * next;
    u8 reserve;
    u8 type;
    u16 size;
}xMemMgrHdr,*pxMemMgrHdr;

typedef struct XMEM_ATTR_PACKED XMEM_ATTR_ALIGNED_4 t_xMemBlock{
    struct t_xMemBlock * next;
    #if XMEM_HEADER_DIVIDE
    void * addr;
    #endif // end of XMEM_HEADER_DIVIDE
    u32 blksize;
    u8 free;
    u8 reserve[3];
}xMemBlock,*pxMemBlock;

typedef struct XMEM_ATTR_PACKED XMEM_ATTR_ALIGNED_4 t_xMemSuperBlock{
    struct t_xMemSuperBlock *next;
    void * addr;
    u16  cellsize;    
    u8  nfree;
    u8  ncell;
    AddressType freeList[__ARRAY_EMPTY];
}xMemSuperBlock;

typedef struct XMEM_ATTR_PACKED XMEM_ATTR_ALIGNED_4 t_xMemSuperBlockInitDesc{
    u16  cellsize;
    u16  ncell;
}xMemSuperBlockInitDesc;

#define XMEM_HEADER_SIZE sizeof( xMemMgrHdr )
#define XMEM_BLOCK_SIZE sizeof( xMemBlock )
#define XMEM_NODE_SIZE( t ) sizeof( t )

#endif // end of __XTYPES_H__
