/*
 * xmem testing
 * Copyright (c) 2016, Shoowing <420260138@qq.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of Apache License Version 2.0
 *
 * See README for more details.
 */
#include <stdio.h>

#include "xtypes.h"
#include "xmem.h"

#define XMEM_TEST_POOL_SIZE     (1024*10)
static unsigned char xmem_test_pool[XMEM_TEST_POOL_SIZE] = {0};

int xmem_test(void )
{
    char * a,*b,*c,*d,*e,*f,*g,*h,*i;

    xMemInit((AddressType)xmem_test_pool,sizeof(xmem_test_pool));
    a=(char *)xmalloc(1);
    b=(char *)xmalloc(5);
    c=(char *)xmalloc(9);
    d=(char *)xmalloc(17);
    xfree(b);
    xMemInfoDump();
    e=(char *)xmalloc(17);
    f=(char *)xmalloc(80);
    g=(char *)xmalloc(20);
    xfree(f);
    xMemInfoDump();
    h=(char *)xmalloc(30);
    i=(char *)xmalloc(50);
    xfree(a);
    xfree(c);
    xMemInfoDump();
    xfree(d);
    xfree(i);
    xfree(h);
    xfree(g);
    xfree(e);
    xMemInfoDump();
    i=(char *)xmalloc(50);
    xMemInfoDump();
    
    return 0;
}
