/******************************************************************************
* Copyright (C) 2019, Huada Semiconductor Co.,Ltd All rights reserved.
*
* This software is owned and published by:
* Huada Semiconductor Co.,Ltd ("HDSC").
*
* BY DOWNLOADING, INSTALLING OR USING THIS SOFTWARE, YOU AGREE TO BE BOUND
* BY ALL THE TERMS AND CONDITIONS OF THIS AGREEMENT.
*
* This software contains source code for use with HDSC
* components. This software is licensed by HDSC to be adapted only
* for use in systems utilizing HDSC components. HDSC shall not be
* responsible for misuse or illegal use of this software for devices not
* supported herein. HDSC is providing this software "AS IS" and will
* not be responsible for issues arising from incorrect user implementation
* of the software.
*
* Disclaimer:
* HDSC MAKES NO WARRANTY, EXPRESS OR IMPLIED, ARISING BY LAW OR OTHERWISE,
* REGARDING THE SOFTWARE (INCLUDING ANY ACOOMPANYING WRITTEN MATERIALS),
* ITS PERFORMANCE OR SUITABILITY FOR YOUR INTENDED USE, INCLUDING,
* WITHOUT LIMITATION, THE IMPLIED WARRANTY OF MERCHANTABILITY, THE IMPLIED
* WARRANTY OF FITNESS FOR A PARTICULAR PURPOSE OR USE, AND THE IMPLIED
* WARRANTY OF NONINFRINGEMENT.
* HDSC SHALL HAVE NO LIABILITY (WHETHER IN CONTRACT, WARRANTY, TORT,
* NEGLIGENCE OR OTHERWISE) FOR ANY DAMAGES WHATSOEVER (INCLUDING, WITHOUT
* LIMITATION, DAMAGES FOR LOSS OF BUSINESS PROFITS, BUSINESS INTERRUPTION,
* LOSS OF BUSINESS INFORMATION, OR OTHER PECUNIARY LOSS) ARISING FROM USE OR
* INABILITY TO USE THE SOFTWARE, INCLUDING, WITHOUT LIMITATION, ANY DIRECT,
* INDIRECT, INCIDENTAL, SPECIAL OR CONSEQUENTIAL DAMAGES OR LOSS OF DATA,
* SAVINGS OR PROFITS,
* EVEN IF Disclaimer HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* YOU ASSUME ALL RESPONSIBILITIES FOR SELECTION OF THE SOFTWARE TO ACHIEVE YOUR
* INTENDED RESULTS, AND FOR THE INSTALLATION OF, USE OF, AND RESULTS OBTAINED
* FROM, THE SOFTWARE.
*
* This software may be replicated in part or whole for the licensed use,
* with the restriction that this Disclaimer and Copyright notice must be
* included with each copy of this software, whether used in part or whole,
* at all times.
*/
/******************************************************************************/
/** \file main.c
 **
 ** A detailed description is available at
 ** @link Sample Group Some description @endlink
 **
 **   - 2019-03-01  1.0  Lux First version
 **
 ******************************************************************************/

/******************************************************************************
 * Include files
 ******************************************************************************/
#include "hc32l19x.h"
#include "base_types.h"
#include "hc32l19x.h"
#include "system_hc32l19x.h"

/******************************************************************************
 * Local pre-processor symbols/macros ('#define')                            
 ******************************************************************************/

/******************************************************************************
 * Global variable definitions (declared in header file with 'extern')
 ******************************************************************************/

/******************************************************************************
 * Local type definitions ('typedef')                                         
 ******************************************************************************/

/******************************************************************************
 * Local function prototypes ('static')
 ******************************************************************************/

/******************************************************************************
 * Local variable definitions ('static')                                      *
 ******************************************************************************/

/******************************************************************************
 * Local pre-processor symbols/macros ('#define')                             
 ******************************************************************************/

/*****************************************************************************
 * Function implementation - global ('extern') and local ('static')
 ******************************************************************************/
 
int fputc(int ch, FILE *f)
{
  M0P_LPUART0->SBUF = (uint8_t)ch;
  while (!M0P_LPUART0->ISR_f.TC);
  M0P_LPUART0->ICR_f.TCCF = 0;
  return ch;
}

static void LPUART0_Config(void)
{
  M0P_SYSCTRL->PERI_CLKEN0_f.LPUART0 = 1;

  M0P_GPIO->PCDIR_f.PC12 = 0; //TXD
	M0P_GPIO->PCDIR_f.PC11 = 1; //RXD
  M0P_GPIO->PCPU_f.PC11 = 1; //RXD
  M0P_GPIO->PC11_SEL = 2;
  M0P_GPIO->PC12_SEL = 1;

  M0P_LPUART0->ICR = 0x00;
  M0P_LPUART0->SCON = 0x00;

  M0P_LPUART0->SCON_f.SCLKSEL = 0; ///*PCLK*///
  M0P_LPUART0->SCON_f.OVER = 2;   ///*4 ??*///
  M0P_LPUART0->SCNT = 13; ///*460800bps*///		

  M0P_LPUART0->SCON_f.SM = 1; ///*mode 1*///
  M0P_LPUART0->SCON_f.B8CONT = 1;
  M0P_LPUART0->SCON_f.RCIE = 1;
  M0P_LPUART0->SADDR = 0x00;
  M0P_LPUART0->SADEN = 0x00;

  M0P_LPUART0->SCON_f.REN = 1;
	NVIC_ClearPendingIRQ(LPUART0_IRQn);
	NVIC_SetPriority(LPUART0_IRQn, 0);
	NVIC_EnableIRQ(LPUART0_IRQn);
}

static void MCU_Config(void)
{
  M0P_SYSCTRL->PERI_CLKEN0_f.GPIO = 1;

  M0P_SYSCTRL->RCL_CR_f.TRIM = (*((volatile uint16_t *)(0x00100C20ul))); //38.4khz

  M0P_SYSCTRL->SYSCTRL2 = 0x5A5A;
  M0P_SYSCTRL->SYSCTRL2 = 0xA5A5;
  M0P_SYSCTRL->SYSCTRL0_f.HCLK_PRS = 7;
  M0P_SYSCTRL->RCH_CR_f.TRIM = (*((volatile uint16_t *)(0x00100C08ul))); //4Mhz	
  M0P_SYSCTRL->RCH_CR_f.TRIM = (*((volatile uint16_t *)(0x00100C06ul))); //8Mhz	
  M0P_SYSCTRL->RCH_CR_f.TRIM = (*((volatile uint16_t *)(0x00100C04ul))); //16Mhz		
  M0P_SYSCTRL->RCH_CR_f.TRIM = (*((volatile uint16_t *)(0x00100C00ul))); //24Mhz	
  M0P_SYSCTRL->SYSCTRL2 = 0x5A5A;
  M0P_SYSCTRL->SYSCTRL2 = 0xA5A5;
  M0P_SYSCTRL->SYSCTRL0_f.HCLK_PRS = 0; 
  SystemCoreClock = 24000000;

  M0P_SYSCTRL->SYSCTRL2 = 0x5A5A;
  M0P_SYSCTRL->SYSCTRL2 = 0xA5A5;
  M0P_SYSCTRL->SYSCTRL0_f.RCL_EN = 1; //RCL enable

  M0P_GPIO->PCADS_f.PC14 = 1;
  M0P_GPIO->PCADS_f.PC15 = 1;

  M0P_SYSCTRL->XTL_CR_f.AMP_SEL = 2;
  M0P_SYSCTRL->XTL_CR_f.DRIVER = 1;
  M0P_SYSCTRL->XTL_CR_f.STARTUP = 3;

  M0P_SYSCTRL->SYSCTRL2 = 0x5A5A;
  M0P_SYSCTRL->SYSCTRL2 = 0xA5A5;
  M0P_SYSCTRL->SYSCTRL0_f.XTL_EN = 1; //X32K enable
  while (M0P_SYSCTRL->XTL_CR_f.STABLE == 0);
}
/**
 ******************************************************************************
 ** \brief  Main function of project
 **
 ** \return uint32_t return value, if needed
 **
 ******************************************************************************/
int32_t main(void)
{
	MCU_Config();
	LPUART0_Config();
	NVIC_ClearPendingIRQ(PORTA_IRQn);
	NVIC_SetPriority(PORTA_IRQn, 3);
	NVIC_EnableIRQ(PORTA_IRQn);	
  printf("HC32L196 Start\r\n");	
	xmem_test();
  while(1);
}

/******************************************************************************
 * EOF (not truncated)
 ******************************************************************************/


